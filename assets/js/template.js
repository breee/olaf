$(document).ready(function() {
  

  var os = navigator.platform;
  if(os == 'iPhone' || os == 'iPad') {
  var tapListener = 'touchstart'
  } else {
    var tapListener = 'click'
  }

  if (screen.width > 769) {
    callPage('./lead-dt.html')
    fetchPageData('lead-dt.html')
  } else {
    callPage('./form.html')
  }
  
  $('a').on('click', function(e) {  
    e.preventDefault()

    var pageRef = $(this).attr('href')
  
    callPage(pageRef)
    
  })

  function callPage(pageRefInput) {
    var leadDataVal = undefined
    var eventDataVal = undefined
    
    $.ajax({
      async: true,
      url: pageRefInput,
      type: 'GET',
      dataType: 'text',
      success: function(res) {
        console.log('Page successfully loaded')
        var script_arr = ['plugins.js', 'main.js']; 

        $.getMultiScripts = function(arr, path) {
            var _arr = $.map(arr, function(scr) {
                return $.getScript( (path||"") + scr );
            });

            _arr.push($.Deferred(function( deferred ){
                $( deferred.resolve );
            }));

            return $.when.apply($, _arr);
        }

        $.getMultiScripts(script_arr, '/assets/js/').done(function() {
        })
        $('.content-bootstrap').html(res)
      },
      error: function(err) {
        console.log('Page note loaded')
      },
      complete: function() {
        console.log('Page request completely loaded')
        fetchPageData(pageRefInput)
      }
    })
  }

  function fetchPageData(pageRefUrl) {
    if($(window).width() == 768) {
      var pageItemSize = '32'
    } else if($(window).width() == 414 ||$(window).width() == 375) {
      var pageItemSize = '8'
    } else {
      var pageItemSize = '64'
    }
    
    if(pageRefUrl == 'lead-dt.html') {
      $.ajax({
        async: true,
        url: 'lead-dt.json',
        type: 'GET',
        dataType: 'json',
        success: function(res) {

          leadDataVal = leadDataConfig(res, pageItemSize)
          $('.card-panel').matchHeight()

          var leadCardPanelId = undefined
          $('.card-panel').on('click', function(e) {
            $('.download-report-fab').addClass('zoomOut')
            $('.download-report-fab').removeClass('zoomIn')
            leadCardPanelId = $(e.target).closest('.card-panel').data('lead_id')
            viewLeadCardPanel(leadCardPanelId)
          })

          $('[data-remodal-id=modal]').remodal()
          $(document).on('confirmation', '.remove-lead-modal', function (e) {
            removeLeadCardPanel(leadCardPanelId)
          })

        },
        error: function(err) {
          console.error(err)
        },
        complete: function() {
          morphCardPanel()
        }
      })
    } else if (pageRefUrl == 'event-dt.html') {
      $.ajax({
        async: true,
        url: 'event-dt.json',
        type: 'GET',
        dataType: 'json',
        success: function(res) {
          
          eventDataVal = eventDataConfig(res, pageItemSize)
          $('.card-panel').matchHeight()

          var eventCardPanelId = undefined
          $('.card-panel').on('click', function(e) {
            $('.download-report-fab').addClass('zoomOut')
            $('.download-report-fab').removeClass('zoomIn')
              eventCardPanelId = $(e.target).closest('.card-panel').data('event_id')
              viewEventCardPanel(eventCardPanelId)
          })

          $('.input-group.date').datepicker({ format: "M dd, yyyy" })
          $('[data-remodal-id=add-event-modal]').remodal()
          $('[data-remodal-id=edit-event-modal]').remodal()
          $('[data-remodal-id=remove-event-modal]').remodal()

          // $(document).on('confirmation', '.add-event-modal', function (e) {
            //Make a function using and ajax with a POST method
            //addEventCardPanel()
          // })

          $(document).on('confirmation', '.edit-event-modal', function (e) {
            //Make a function using and ajax with a POST method
            //addEventCardPanel()
          })

          $(document).on('confirmation', '.remove-event-modal', function (e) {
            removeEventCardPanel(eventCardPanelId)
          })

        },
        error: function(err) {
          console.error(err)
        },
        complete: function() {
          morphCardPanel()
        }
      })
    }
  }

  /* ListJS Lead Data Config */
  function leadDataConfig(responseData, pageItemSize) {
    var options = {
      item: `<li class="col-xs-6 col-sm-3 card-panel force-acceleration">
          <section class="data-holder">
            <section class="data-holder-content clearfix">
              <span class="date pull-left"></span>
              <span class="lead_strength pull-right"></span>
              <h3 class="name"></h3>
              <p class="lead_from"></p>
            </section>
            <span class="cd-modal-bg"></span>
          </section>
        </li>`,
      valueNames: [ 
        { 'data' : ['lead_id'] },
        { name: 'lead_strength', attr: 'data-lead_strength' }, 
        'name', 
        'date', 
        'lead_from'
      ],
      page: pageItemSize,
      pagination: true
    }

    var values = responseData

    return new List('lead-list', options, values)
  }

  function eventDataConfig(responseData, pageItemSize) {

    var options = {
      item: `<li class="col-xs-6 col-sm-3 card-panel force-acceleration">
          <section class="data-holder">
            <section class="data-holder-content clearfix">
              <span class="date pull-left"></span>
              <h3 class="event_name"></h3>
              <p class="country"></p>
            </section>
            <span class="cd-modal-bg"></span>
          </section>
        </li>`,
      valueNames: [ 
        { 'data' : ['event_id'] },
        'event_name', 
        'date', 
        'country'
      ],
      page: pageItemSize,
      pagination: true
    }

    var values = responseData

    return new List('event-list', options, values)
  }

  /* ListJS Method Calls */
  function viewLeadCardPanel(leadCardPanelIdRef) {
    var objData = leadDataVal.get("lead_id", leadCardPanelIdRef)
    $('.panel-name').html(objData[0]._values.name)
    $('.panel-date').html(objData[0]._values.date)
    $('.panel-lead-from').html(objData[0]._values.lead_from)
    $('.panel-hubspot-entry').html(objData[0]._values.hubspot_entry)
    $('.panel-more-notes').html(objData[0]._values.more_notes)
    
    $('.image-holder img').attr('src', objData[0]._values.img_url)
    $('.lead-strength').addClass(objData[0]._values.lead_strength)

    var contactDetails = objData[0]._values.contact_details[0]
    var panelContactDetailsTemplate = $('#panel-contact-details').html()
    Mustache.parse(panelContactDetailsTemplate)
    $('#contact-details').html(Mustache.render(panelContactDetailsTemplate, contactDetails))

    var businessTypeDetails = objData[0]._values.business_type
    var panelBusinessTypeTemplate = $('#panel-business-type').html()
    Mustache.parse(panelBusinessTypeTemplate)
    $('#business-type').html(Mustache.render(panelBusinessTypeTemplate, { details: businessTypeDetails }))

    var serviceInterestedDetails = objData[0]._values.service_interested
    var panelServiceInterestedTemplate = $('#panel-service-interested').html()
    Mustache.parse(panelServiceInterestedTemplate)
    $('#service-interested').html(Mustache.render(panelBusinessTypeTemplate, { details: serviceInterestedDetails }))

    $('.panel-business-type').html(objData[0]._values.business_type_notes)
    $('.panel-service-interested').html(objData[0]._values.service_interested_notes)
  }

  function removeLeadCardPanel(leadRemoveCardPanelIdRef) {
    $.ajax({
      async: true,
      // change this path to delete url e.g. /delete_lead_id or remove "data" property 
      // and just pass along the id at the endpoint 
      // e.g. /delete_lead_id/leadRemoveCardPanelIdRef
      url: 'delete-url.json', 
      type: 'GET', // change the type to 'DELETE'
      dataType: 'json',
      data: { leadId: leadRemoveCardPanelIdRef } ,
      contentType: 'application/json; charset=utf-8',
      success: function(res) {
        leadDataVal.remove("lead_id", leadRemoveCardPanelIdRef)

      },
      error: function(err) {
        console.error(err)
      },
      complete: function() {
        console.log('Successfully Deleted')
      }
    })
  }

  function removeEventCardPanel(eventRemoveCardPanelIdRef) {
    $.ajax({
      async: true,
      // change this path to delete url e.g. /delete_lead_id or remove "data" property 
      // and just pass along the id at the endpoint 
      // e.g. /delete_lead_id/leadRemoveCardPanelIdRef
      url: 'delete-url.json', 
      type: 'GET', // change the type to 'DELETE'
      dataType: 'json',
      data: { eventId: eventRemoveCardPanelIdRef } ,
      contentType: 'application/json; charset=utf-8',
      success: function(res) {
        eventDataVal.remove("event_id", eventRemoveCardPanelIdRef)

      },
      error: function(err) {
        console.error(err)
      },
      complete: function() {
        console.log('Successfully Deleted Event')
      }
    })
  }

  function viewEventCardPanel(eventCardPanelIdRef) {
    var objData = eventDataVal.get("event_id", eventCardPanelIdRef)
    $('.event-name').html(objData[0]._values.event_name)
    $('.event-date').html(objData[0]._values.date)
    $('.event-country').html(objData[0]._values.country)

    $('.edit-event-name').attr('placeholder', objData[0]._values.event_name)
    $('.edit-event-date').attr('placeholder', objData[0]._values.date)
    $('.edit-event-country').attr('placeholder', objData[0]._values.country)
  }

  function morphCardPanel() {

    //trigger the animation - open modal window
    $('.card-panel').on('click', function(e) {      
      var actionBtn  = $(e.target).closest('.card-panel'),
          scaleValue = retrieveScale(actionBtn.find('.cd-modal-bg'))

          // actionBtn.find('.data-holder-content').addClass('hide-content')
      
      var cardPanelSequence = [{
        e: actionBtn.find('.data-holder-content'),
        p: {
          opacity: 0,
          height: 0,
        },
        o: {
          duration: 100,
          easing: "easeInQuad"
        }
      },{
        e: actionBtn.find('.data-holder'),
        p: {
          width: '6em',
          height: '6em',
          borderRadius: '50%'
        },
        o: {
          duration: 100,
          easing: "easeInQuad"
        }
      }]

      if (os === 'iPad') {
        actionBtn.find('.data-holder-content').removeClass('ipad-fadein')
        actionBtn.find('.data-holder-content').addClass('ipad-fadeout')
      } else {
        $.Velocity.RunSequence(cardPanelSequence)
      }

      actionBtn.find('.cd-modal-bg').addClass('is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
        setTimeout(function() {
          animateLayer(actionBtn.find('.cd-modal-bg'), scaleValue, true)
        }, 300)
        $('.modal-fab').addClass('zoomIn')
        $('.modal-fab').removeClass('zoomOut')
        $('body, .card-list').addClass('overflow-hidden')
        // $('.pagination').velocity('fadeOut', 300)
        // $('.card-list .list, .card-list .page-header').velocity({
        //   height: 0
        // },{
        //   delay: 300,
        //   duration: 300
        // })
      })
      
    })

    //trigger the animation - close modal window
    $('.cd-modal-close, .card-pop-modal [data-remodal-action="confirm"]').on('click', function() {
      var section = $('.card-list')
      section.removeClass('modal-is-visible')
      // .one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
        $('body, .card-list').removeClass('overflow-hidden')
        $('.modal-fab').removeClass('zoomIn')
        $('.modal-fab').addClass('zoomOut')
        $('.download-report-fab').removeClass('zoomOut')
        $('.download-report-fab').addClass('zoomIn')
        // $('.pagination').velocity('fadeIn', 300)

        setTimeout(function() {
          animateLayer(section.find('.cd-modal-bg'), 1, false)
        }, 300)
      // })
    })

  }
    
    function retrieveScale(btn) {
      var addHeight = 0 
      if(os == 'iPad')
        addHeight = 700 

      var btnRadius = btn.width()/2,
        left = btn.offset().left + btnRadius,
        top = btn.offset().top + btnRadius - $(window).scrollTop(),
        scale = scaleValue(top, left, btnRadius, $(window).height() + addHeight, $(window).width())

      btn.css('position', 'fixed').velocity({
        top: (top - btnRadius) - 28,
			  left: (left - btnRadius) - 28,
			  translateX: 0,
      }, 0)

      return scale
    }
    
    function scaleValue( topValue, leftValue, radiusValue, windowW, windowH) {
        var maxDistHor = ( leftValue > windowW/2) ? leftValue : (windowW - leftValue),
          maxDistVert = ( topValue > windowH/2) ? topValue : (windowH - topValue)
          
        return Math.ceil(Math.sqrt( Math.pow(maxDistHor, 2) + Math.pow(maxDistVert, 2) )/radiusValue)
      }
      
      function animateLayer(layer, scaleVal, bool) {
        console.log('qwe')
        layer.velocity({ scale: scaleVal }, 300 , function(){
          if (bool) {
            console.log('open')
            layer.parents('.card-list').addClass('modal-is-visible').end().off('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend')
          } else {
            console.log('close')
            layer.removeClass('is-visible').removeAttr( 'style' )

            // $('.card-panel [style]').removeAttr('style')
            var cardPanelCloseSequence = [{
              e: $('.card-panel').find('.data-holder'),
              p: {
                width: '100%',
                height: '100%',
                borderRadius: '2px'
              },
              o: {
                duration: 100,
                easing: "easeInQuad"
              }
            },{
              e: $('.card-panel').find('.data-holder-content'),
              p: {
                opacity: 1,
                height: '100%',
              },
              o: {
                delay: 500,
                duration: 200,
                easing: "easeInQuad"
              }
            }]

            if (os === 'iPad') {
              $('.card-panel').find('.data-holder-content').removeClass('ipad-fadeout')
              $('.card-panel').find('.data-holder-content').addClass('ipad-fadein')
            } else {
              $.Velocity.RunSequence(cardPanelCloseSequence)
            }

            
            $('body, .card-list').removeClass('overflow-hidden')
            // $('.pagination').velocity('fadeIn', 300)
            // $('.card-list .list, .card-list .page-header').velocity({
            //   height: '100%'
            // },{
            //   delay: 300,
            //   duration: 300
            // })
          } 

        })
      }

})