$(document).ready(function() {
  var os = navigator.platform;
  if(os == 'iPhone' || os == 'iPad') {
    $('.navbar-inverse').addClass('padding-top-ios')
    var tapListener = 'touchstart'
  } else {
    var tapListener = 'click'
  }

  $('.pill:not(:last-child) .nice-select').click(function(e) {
    $('.nav-pill-holder').toggleClass('active')
    if($(this).hasClass('open') === false) {
      $(this).closest('.pill:not(:last-child)').addClass('active')
    } else {
      $(this).closest('.pill').removeClass('active')
    }
  })

  $(document).on('click.nice_select', function(event) {
    if($(event.target).closest('.nice-select').length === 0){
      $('.nav-pill-holder').removeClass('active')
      $('.nav-pill-holder .pill').removeClass('active')
    }
  })

 $(document).on('click.nice_select', '.nice-select', function(event) {
    if(($(event.target).closest('.nav-pill-holder.active').length === 0 || $(event.target).closest('.pill.pill-button').length === 0)){
      $('.nav-pill-holder').addClass('active')
      $('.nav-pill-holder .pill').removeClass('active')

      if($(event.target).closest('.nice-select').hasClass('open')) {
        $(event.target).closest('.nice-select').closest('.pill:not(:last-child)').addClass('active')
      } else {
        $(event.target).closest('.nice-select').closest('.pill').removeClass('active')
      }
    }
  })

  $('input:checkbox').on('change', function () {
    $(this).parent().toggleClass("checked")
  })

  $('.other-business-type-field input[type="text"], .other-service-interested-field input[type="text"]').blur(function(){
    tmpval = $(this).val();
    if(tmpval == '') {
      $(this).removeClass('has-value')
    } else {
      $(this).addClass('has-value')
    }
  })

  $('.business-type-fab .note-actions').hide()
  $('.service-interested-fab .note-actions').hide()
  $('.countries-active-fab .note-actions').hide()

  $(document).on(tapListener, '.business-type-fab', function (e) {
    if($(e.target).closest('.note-actions').length === 0) {
      $(this).addClass('to-expand')
      $('.business-type-fab.to-expand textarea').addClass('show-textarea')
      $('.business-type-fab.to-expand .icon-note_add').hide()
      $('.business-type-fab .note-actions').show()
    }
  })

  $(document).on(tapListener, ".business-type-fab .note-actions", function (e) {
    if($(e.target).closest('.note-actions').length === 1) {
      $(this).closest('.business-type-fab').removeClass('to-expand')
      $('.business-type-fab textarea').removeClass('show-textarea')
      $('.business-type-fab .note-actions').hide()
      $('.business-type-fab .icon-note_add').show()
    }
  })

  $(document).on(tapListener, '.service-interested-fab', function (e) {
    if($(e.target).closest('.note-actions').length === 0) {
      $(this).addClass('to-expand')
      $('.service-interested-fab.to-expand textarea').addClass('show-textarea')
      $('.service-interested-fab.to-expand .icon-note_add').hide()
      $('.service-interested-fab .note-actions').show()
    }
  })

  $(document).on(tapListener, ".service-interested-fab .note-actions", function (e) {
    if($(e.target).closest('.note-actions').length === 1) {
      $(this).closest('.service-interested-fab').removeClass('to-expand')
      $('.service-interested-fab textarea').removeClass('show-textarea')
      $('.service-interested-fab .note-actions').hide()
      $('.service-interested-fab .icon-note_add').show()
    }
  })

  $(document).on(tapListener, '.countries-active-fab', function (e) {
    if($(e.target).closest('.note-actions').length === 0) {
      $(this).addClass('to-expand')
      $('.countries-active-fab.to-expand textarea').addClass('show-textarea')
      $('.countries-active-fab.to-expand .icon-note_add').hide()
      $('.countries-active-fab .note-actions').show()
    }
  })

  $(document).on(tapListener, ".countries-active-fab .note-actions", function (e) {
    if($(e.target).closest('.note-actions').length === 1) {
      $(this).closest('.countries-active-fab').removeClass('to-expand')
      $('.countries-active-fab textarea').removeClass('show-textarea')
      $('.countries-active-fab .note-actions').hide()
      $('.countries-active-fab .icon-note_add').show()
    }
  })

  $('.home-form-action-buttons textarea').hide()
  $('.more-notes-fab').hide()
  $('.more-notes-btn').on(tapListener, function() {
    $(this).closest('.home-form-action-buttons').addClass('more-notes-expanded')

    setTimeout(function() {
      $('.more-notes-btn, .more-notes-submit').removeClass('zoomIn')
      $('.more-notes-btn, .more-notes-submit').addClass('animated zoomOut')
      $('.more-notes-fab').show()
      $('.more-notes-fab').removeClass('zoomOut')
      $('.more-notes-fab').addClass('animated zoomIn')
    }, 500)

    setTimeout(function() {
      $('.home-form-action-buttons textarea').show()
      $('.home-form-action-buttons textarea').removeClass('fadeOut')
      $('.home-form-action-buttons textarea').addClass('fadeIn')
    }, 1500)
  })

  $('.more-notes-fab').on(tapListener, function() {
    setTimeout(function() {
      $('.more-notes-btn, .more-notes-submit').removeClass('animated zoomOut')
      $('.more-notes-btn, .more-notes-submit').addClass('animated zoomIn')
      $('.home-form-action-buttons textarea').removeClass('fadeIn')
      $('.home-form-action-buttons textarea').addClass('fadeOut')
      $('.more-notes-fab').removeClass('animated zoomIn')
      $('.more-notes-fab').addClass('animated zoomOut')
        // setTimeout(function() {
        //   $('.more-notes-fab').fadeOut()
        // }, 400)
    }, 500)

    setTimeout(function() {
      $('.home-form-action-buttons').removeClass('more-notes-expanded')
    }, 1250)
  })

  $('.sort-holder .sort').on('click', function(e) {
    $(e.target).toggleClass('sort-out')
  })

  $('.pagination').each(function(){
    if($(this).find('li').length == 1){
      $(this).hide()
    }
  })

  if(screen.width > 768) {
    $('.lead-grid').addClass('container')
  }
      
    // setTimeout(function() {
    //   $('.lead-list .page-header').velocity('fadeOut', 600)
    //   $('.cd-modal').siblings().not('.cd-modal').velocity('fadeOut', 600)
    // }, 600)
  // $('.lead-dt-btn').on(tapListener, function() {
  //   $('.content-boostrap').load('lead-dt.html')
  // })
})